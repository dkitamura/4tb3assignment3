ANTLR4=java -Xmx500M -cp "/usr/local/lib/antlr-4.5-complete.jar:$$CLASSPATH" org.antlr.v4.Tool
GRUN=java org.antlr.v4.runtime.misc.TestRig

default: 
	$(ANTLR4) table_formatter_0854359.g4
	javac table_formatter*.java
