// Generated from table_formatter_0854359.g4 by ANTLR 4.5
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link table_formatter_0854359Parser}.
 */
public interface table_formatter_0854359Listener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link table_formatter_0854359Parser#r}.
	 * @param ctx the parse tree
	 */
	void enterR(table_formatter_0854359Parser.RContext ctx);
	/**
	 * Exit a parse tree produced by {@link table_formatter_0854359Parser#r}.
	 * @param ctx the parse tree
	 */
	void exitR(table_formatter_0854359Parser.RContext ctx);
	/**
	 * Enter a parse tree produced by {@link table_formatter_0854359Parser#row}.
	 * @param ctx the parse tree
	 */
	void enterRow(table_formatter_0854359Parser.RowContext ctx);
	/**
	 * Exit a parse tree produced by {@link table_formatter_0854359Parser#row}.
	 * @param ctx the parse tree
	 */
	void exitRow(table_formatter_0854359Parser.RowContext ctx);
	/**
	 * Enter a parse tree produced by {@link table_formatter_0854359Parser#col}.
	 * @param ctx the parse tree
	 */
	void enterCol(table_formatter_0854359Parser.ColContext ctx);
	/**
	 * Exit a parse tree produced by {@link table_formatter_0854359Parser#col}.
	 * @param ctx the parse tree
	 */
	void exitCol(table_formatter_0854359Parser.ColContext ctx);
}