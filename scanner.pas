unit scanner;
interface
    uses sysutils;

  const
    IdLen = 255; {Max number of characters in contents}

  type
    Symbol = (null, TableOpenSym, TableCloseSym, RowOpenSym, RowCloseSym,
              ColOpenSym, ColCloseSym, UnknownTagSym, ContentSym, EofSym);
    Contents = string[IdLen];

  var
    sym: Symbol; {next symbol}
    val: integer; {value of number if sym = NumberSym}
    conts: Contents; {string for contents if sym = ContentSym}
    tag: Contents; {string for tag if sym = UnknownTagSym}
    error: Boolean; {whether an error has occurred so far}

  procedure Mark (msg: string);

  procedure GetSym;

implementation

  const
    KW = 6; {Number of keywords}

  type
    KeyTable = array [1..KW] of
      record sym: Symbol; conts: Contents end;

  var
    ch: char;
    line, lastline, errline: integer;
    pos, lastpos, errpos: integer;
    keyTab: KeyTable;
    fn: string[255]; {name of source file}
    source: text; {source file}

  procedure GetChar;
  begin
    lastpos := pos;
    if eoln (source) then begin pos := 0; line := line + 1 end
    else begin lastline:= line; pos := pos + 1 end;
    read (source, ch)
  end;

  procedure GetTag;
    var len, k: integer;
  begin len := 0;
    GetChar;
    repeat
      if len < IdLen then begin len := len + 1; tag[len] := ch; end;
      GetChar
    until (ch =  '>');
    GetChar;
    setlength(tag, len); k := 1;
    while (k <= KW) and not (CompareText(tag, keyTab[k].conts) = 0) do k := k + 1;
    if k <= KW then sym := keyTab[k].sym else sym := UnknownTagSym
  end;

  procedure GetContents;
    var len, k: integer;
  begin len := 0;
    repeat
      if len < IdLen then begin len := len + 1; conts[len] := ch; end;
      GetChar
    until not (ch in  ['A'..'Z', 'a'..'z', '0'..'9']);
    setlength(conts, len); k := 1;
    while (k <= KW) and (conts <> keyTab[k].conts) do k := k + 1;
    if k <= KW then sym := keyTab[k].sym else sym := ContentSym
  end;

  procedure Mark (msg: string);
  begin
    if (lastline > errline) or (lastpos > errpos) then
      writeln ('error: line ', lastline:1, ' pos ', lastpos:1, ' ', msg);
    errline := lastline; errpos := lastpos; error := true
  end;

  procedure GetSym;
  begin {first skip white space}
    while not eof (source) and (ch <= ' ') and not error do GetChar;
    if eof (source) then sym := EofSym
    else
      case ch of
        '<': GetTag;
        'A' .. 'Z', 'a'..'z', '0'..'9': GetContents;
      otherwise
        begin GetChar; sym := null end
      end;
  end;

begin
  line := 1; lastline := 1; errline := 1;
  pos := 0; lastpos := 0; errpos := 0;
  error := false;
  keyTab[1].sym := TableOpenSym; keyTab[1].conts := 'table';
  keyTab[2].sym := TableCloseSym; keyTab[2].conts := '/table';
  keyTab[3].sym := RowOpenSym; keyTab[3].conts := 'tr';
  keyTab[4].sym := RowCloseSym; keyTab[4].conts := '/tr';
  keyTab[5].sym := ColOpenSym; keyTab[5].conts := 'td';
  keyTab[6].sym := ColCloseSym; keyTab[6].conts := '/td';
  if paramcount > 0 then
    begin fn := paramstr (1); assign (source, fn); reset (source);
      GetChar
    end
  else Mark ('name of source file expected')
end.
