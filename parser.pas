unit parser;
interface
    uses scanner;

    type contents = string[255];
    type node = record child: ^node; next: ^node; cont: contents end;
    type nodePtr = ^node;
    var parseTree : nodePtr;

    procedure Parse;

implementation
    type ParseState = (ParseOutside, ParseTable, ParseRow, ParseColumn);

    var currentTable : nodePtr;
    var currentRow : nodePtr;
    var currentCol : nodePtr;

    var newNode : nodePtr;
    var hasError : boolean;
    var state : ParseState;

    procedure addChild(holder, item: nodePtr);
        var itNode: nodePtr;
    begin
        itNode := holder^.child;
        if (itNode = nil) then
            holder^.child := item
        else
            begin
                while not (itNode^.next = nil) do
                begin
                    itNode := itNode^.next;
                end;
                itNode^.next := item;
            end;
    end;

    procedure parseError(errString: string);
    begin
        Mark(errstring);
        hasError := true;
    end;

    (* program entry *)
    procedure Parse;
    begin
        state := ParseOutside;
        currentTable := nil;
        currentRow := nil;
        currentCol := nil;
        hasError := false;

        GetSym;
        while (not (sym = EofSym)) and (not hasError) do
        begin
            case sym of
                { enter and leave table }
                TableOpenSym:
                begin
                    if (state = ParseOutside) then
                    begin
                        state := ParseTable;

                        New(newNode);
                        currentTable := newNode;
                        currentTable^.child := nil;
                        currentTable^.next := nil;
                        currentTable^.cont := '';
                    end
                    else
                        parseError('unexpected <table> tag.');
                end;
                TableCloseSym:
                begin
                    if (state = ParseTable) then
                    begin
                        state := ParseOutside;
                    end
                    else
                        parseError('unexpected </table> tag.');
                end;

                { enter and leave row }
                RowOpenSym:
                begin
                    if (state = ParseTable) then
                    begin
                        state := ParseRow;
                        New(newNode);
                        newNode^.child := nil;
                        newNode^.next := nil;
                        currentRow := newNode;
                        addChild(currentTable, currentRow);
                    end
                    else
                        parseError('unexpected <tr> tag.');
                end;
                RowCloseSym:
                begin
                    if (state = ParseRow) then
                    begin
                        state := ParseTable;
                    end
                    else
                        parseError('unexpected </tr> tag.');
                end;

                { enter and leave column }
                ColOpenSym:
                begin
                    if (state = ParseRow) then
                    begin
                        state := ParseColumn;
                        New(newNode);
                        newNode^.child := nil;
                        newNode^.next := nil;
                        currentCol := newNode;
                        addChild(currentRow, currentCol);
                    end
                    else
                        parseError('unexpected <td> tag.');
                end;
                ColCloseSym:
                begin
                    if (state = ParseColumn) then
                    begin
                        state := ParseRow;
                    end
                    else
                        parseError('unexpected </td> tag.');
                end;

                { column contents }
                ContentSym:
                begin
                    if (state = ParseColumn) then
                    begin
                        currentCol^.cont := conts;
                    end
                end;
                otherwise
                begin
                end;
            end;

            { next symbol }
            GetSym;
        end;

        if (hasError = true) then
            parseTree := nil
        else
            parseTree := currentTable;
    end;

begin
    currentTable := nil;
    currentRow := nil;
    currentCol := nil;
    parseTree := nil;
end.
