//Darren Kitamura
//0854359

grammar table_formatter_0854359;

r
locals[ int currentRow = 0,
	int currentCol = 0,
	int maxRow = 0,
	int maxCol = 0,
	int tableDataLength = 0,
	List<Integer> rowWidth = new ArrayList<Integer>(),
	List<Integer> colWidth = new ArrayList<Integer>(),
	List<List<String>> tableData = new ArrayList<List<String>>()

] : TableOpen row* TableClose {
	
	if($r::tableData.size() == 0) {
		System.out.println("No Table Data");
	}
	else {
		for(int i = 0; i < $r::tableData.size(); i++) {
			System.out.print("| ");
			for(int j = 0; j < $r::tableData.get(i).size(); j++) {

				System.out.print($r::tableData.get(i).get(j));
				for(int k = $r::tableData.get(i).get(j).length(); k< $r::tableDataLength; k++) {
					System.out.print("_");				
				}
			System.out.print(" | ");
			}
			System.out.print("\n");
	
		}	
	}
};

row : RowOpen col* RowClose {
	
	if($r::tableData.size() <= $r::currentRow) {
		$r::tableData.add(new ArrayList<String>());		
	}
	
	$r::currentRow++;
	$r::currentCol = 0;
};

col : ColOpen TableContents ColClose {

	//Strip spaces
	String temp = $TableContents.text;
	temp = temp.replaceAll("\\s","");

	//Set largest number of characters
	if(temp.length() > $r::tableDataLength) {
		$r::tableDataLength = temp.length();
	}



	if($r::currentCol == 0) {
		$r::tableData.add(new ArrayList<String>());	
	}
	
		$r::tableData.get($r::currentRow).add(temp);
		$r::currentCol++;
	
};

TableOpen : '<'('t'|'T')('a'|'A')('b'|'B')('l'|'L')('e'|'E')'>';
TableClose : '</'('t'|'T')('a'|'A')('b'|'B')('l'|'L')('e'|'E')'>';

RowOpen : '<'('t'|'T')('r'|'R')'>';
RowClose : '</'('t'|'T')('r'|'R')'>';

ColOpen : '<'('t'|'T')('d'|'D')'>';
ColClose : '</'('t'|'T')('d'|'D')'>';

TableContents: CONTENTS+;

fragment CONTENTS : ~[<\r\n]+;
WS : [\t\r\n]+ -> skip;//Skip spaces, tabs, new lines
