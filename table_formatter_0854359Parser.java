// Generated from table_formatter_0854359.g4 by ANTLR 4.5
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class table_formatter_0854359Parser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		TableOpen=1, TableClose=2, RowOpen=3, RowClose=4, ColOpen=5, ColClose=6, 
		TableContents=7, WS=8;
	public static final int
		RULE_r = 0, RULE_row = 1, RULE_col = 2;
	public static final String[] ruleNames = {
		"r", "row", "col"
	};

	private static final String[] _LITERAL_NAMES = {
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "TableOpen", "TableClose", "RowOpen", "RowClose", "ColOpen", "ColClose", 
		"TableContents", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "table_formatter_0854359.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public table_formatter_0854359Parser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class RContext extends ParserRuleContext {
		public int currentRow =  0;
		public int currentCol =  0;
		public int maxRow =  0;
		public int maxCol =  0;
		public int tableDataLength =  0;
		public List<Integer> rowWidth =  new ArrayList<Integer>();
		public List<Integer> colWidth =  new ArrayList<Integer>();
		public List<List<String>> tableData =  new ArrayList<List<String>>();
		public TerminalNode TableOpen() { return getToken(table_formatter_0854359Parser.TableOpen, 0); }
		public TerminalNode TableClose() { return getToken(table_formatter_0854359Parser.TableClose, 0); }
		public List<RowContext> row() {
			return getRuleContexts(RowContext.class);
		}
		public RowContext row(int i) {
			return getRuleContext(RowContext.class,i);
		}
		public RContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_r; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof table_formatter_0854359Listener ) ((table_formatter_0854359Listener)listener).enterR(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof table_formatter_0854359Listener ) ((table_formatter_0854359Listener)listener).exitR(this);
		}
	}

	public final RContext r() throws RecognitionException {
		RContext _localctx = new RContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_r);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(6);
			match(TableOpen);
			setState(10);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==RowOpen) {
				{
				{
				setState(7);
				row();
				}
				}
				setState(12);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(13);
			match(TableClose);

				
				if(((RContext)getInvokingContext(0)).tableData.size() == 0) {
					System.out.println("No Table Data");
				}
				else {
					for(int i = 0; i < ((RContext)getInvokingContext(0)).tableData.size(); i++) {
						System.out.print("| ");
						for(int j = 0; j < ((RContext)getInvokingContext(0)).tableData.get(i).size(); j++) {

							System.out.print(((RContext)getInvokingContext(0)).tableData.get(i).get(j));
							for(int k = ((RContext)getInvokingContext(0)).tableData.get(i).get(j).length(); k< ((RContext)getInvokingContext(0)).tableDataLength; k++) {
								System.out.print("_");				
							}
						System.out.print(" | ");
						}
						System.out.print("\n");
				
					}	
				}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RowContext extends ParserRuleContext {
		public TerminalNode RowOpen() { return getToken(table_formatter_0854359Parser.RowOpen, 0); }
		public TerminalNode RowClose() { return getToken(table_formatter_0854359Parser.RowClose, 0); }
		public List<ColContext> col() {
			return getRuleContexts(ColContext.class);
		}
		public ColContext col(int i) {
			return getRuleContext(ColContext.class,i);
		}
		public RowContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_row; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof table_formatter_0854359Listener ) ((table_formatter_0854359Listener)listener).enterRow(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof table_formatter_0854359Listener ) ((table_formatter_0854359Listener)listener).exitRow(this);
		}
	}

	public final RowContext row() throws RecognitionException {
		RowContext _localctx = new RowContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_row);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(16);
			match(RowOpen);
			setState(20);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ColOpen) {
				{
				{
				setState(17);
				col();
				}
				}
				setState(22);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(23);
			match(RowClose);

				
				if(((RContext)getInvokingContext(0)).tableData.size() <= ((RContext)getInvokingContext(0)).currentRow) {
					((RContext)getInvokingContext(0)).tableData.add(new ArrayList<String>());		
				}
				
				((RContext)getInvokingContext(0)).currentRow++;
				((RContext)getInvokingContext(0)).currentCol =  0;

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColContext extends ParserRuleContext {
		public Token TableContents;
		public TerminalNode ColOpen() { return getToken(table_formatter_0854359Parser.ColOpen, 0); }
		public TerminalNode TableContents() { return getToken(table_formatter_0854359Parser.TableContents, 0); }
		public TerminalNode ColClose() { return getToken(table_formatter_0854359Parser.ColClose, 0); }
		public ColContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_col; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof table_formatter_0854359Listener ) ((table_formatter_0854359Listener)listener).enterCol(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof table_formatter_0854359Listener ) ((table_formatter_0854359Listener)listener).exitCol(this);
		}
	}

	public final ColContext col() throws RecognitionException {
		ColContext _localctx = new ColContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_col);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(26);
			match(ColOpen);
			setState(27);
			((ColContext)_localctx).TableContents = match(TableContents);
			setState(28);
			match(ColClose);


				//Strip spaces
				String temp = (((ColContext)_localctx).TableContents!=null?((ColContext)_localctx).TableContents.getText():null);
				temp = temp.replaceAll("\\s","");

				//Set largest number of characters
				if(temp.length() > ((RContext)getInvokingContext(0)).tableDataLength) {
					((RContext)getInvokingContext(0)).tableDataLength =  temp.length();
				}



				if(((RContext)getInvokingContext(0)).currentCol == 0) {
					((RContext)getInvokingContext(0)).tableData.add(new ArrayList<String>());	
				}
				
					((RContext)getInvokingContext(0)).tableData.get(((RContext)getInvokingContext(0)).currentRow).add(temp);
					((RContext)getInvokingContext(0)).currentCol++;
				

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\n\"\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\3\2\3\2\7\2\13\n\2\f\2\16\2\16\13\2\3\2\3\2\3\2\3\3\3\3\7\3"+
		"\25\n\3\f\3\16\3\30\13\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\2\2\5\2\4"+
		"\6\2\2 \2\b\3\2\2\2\4\22\3\2\2\2\6\34\3\2\2\2\b\f\7\3\2\2\t\13\5\4\3\2"+
		"\n\t\3\2\2\2\13\16\3\2\2\2\f\n\3\2\2\2\f\r\3\2\2\2\r\17\3\2\2\2\16\f\3"+
		"\2\2\2\17\20\7\4\2\2\20\21\b\2\1\2\21\3\3\2\2\2\22\26\7\5\2\2\23\25\5"+
		"\6\4\2\24\23\3\2\2\2\25\30\3\2\2\2\26\24\3\2\2\2\26\27\3\2\2\2\27\31\3"+
		"\2\2\2\30\26\3\2\2\2\31\32\7\6\2\2\32\33\b\3\1\2\33\5\3\2\2\2\34\35\7"+
		"\7\2\2\35\36\7\t\2\2\36\37\7\b\2\2\37 \b\4\1\2 \7\3\2\2\2\4\f\26";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}